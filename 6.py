def func(dict_one, dict_two):
    dict_one.update(dict_two)
    return dict_one
dict_one = {
    "id": 1,
    "name": "Ashutosh",
}
dict_two = {
    "books": ["Python", "DSA"],
    "college": "NSEC",
}
merged_dict = func(dict_one, dict_two)
print(merged_dict)