import time
def decorator(function):
    def the_wrapper():
        start = time.time()
        function()
        end = time.time()
        print("Function was running for: " + str(end- start))
    return the_wrapper
@decorator
def function1():
    time.sleep(1)
    for a in range (1, 10):
        a = a + 1
    print("smth")
function1()