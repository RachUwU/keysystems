import datetime
def date(year, month, day):
    try:
        datetime.datetime(year=year, month=month, day=day)
        return True
    except ValueError:
        return False
print(date(2020, 12, 1))
print(date(2020, 22, 1))