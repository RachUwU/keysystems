import math
class GeometricShapes:
    regularpoligon = True
    def __init__(self, angles, radiusvector):
        self.angles = angles
        self.radiusvector = radiusvector
    def __str__(self):
        return f"Geometric shape with {self.angles} angles and radius = {self.radiusvector}"

class Triangles(GeometricShapes):
    angles = 3
    def __init__(self, radiusvector, color = "Green"):
        self.radiusvector = radiusvector
        self.color = color
    def __str__(self):
        return f"{self.color} triangle with radius = {self.radiusvector}"
    def square(self):
        return 3*math.sqrt(3)*(self.radiusvector**2)/4
    def describe(self):
        return super().__str__()
a = Triangles(4)
print(a)
print(a.describe())
print(isinstance(a, GeometricShapes))